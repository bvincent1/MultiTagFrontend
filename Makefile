ifndef CI
	include .env
endif

install:
	yarn install

run:
	yarn start

deploy:
	aws s3 sync public/ s3://$(AWS_BUCKET)
