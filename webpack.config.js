const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const webpack = require('webpack');
require('dotenv').config()

const ENV = process.env.APP_ENV;
const isTest = ENV === 'test'
const isProd = ENV === 'prod';

function setDevTool() {
    if (isTest) {
      return 'inline-source-map';
    } else if (isProd) {
      return 'source-map';
    } else {
      return 'eval-source-map';
    }
}

const config = {
  entry: __dirname + "/src/app/index.js",
  output: {
    path: __dirname + '/public',
    filename: 'bundle.js',
    publicPath: '/',
    pathinfo: true
  },
  devtool: setDevTool(),
  module: {
      rules: [
          {
            test: /\.js$/,
            use: 'babel-loader',
            exclude: [
              /node_modules/
            ]
        },
          {
              test: /\.html/,
              loader: 'raw-loader'
          },
          {
            test: /\.(sass|scss)$/,
            use: [{
                loader: "style-loader" // creates style nodes from JS strings
            }, {
                loader: "css-loader" // translates CSS into CommonJS
            }, {
                loader: "sass-loader" // compiles Sass to CSS
            }]
          }
      ]
  },
  plugins: [
      new HtmlWebpackPlugin({
          template: __dirname + "/public/index.html",
          inject: 'body'
      }),
      new webpack.DefinePlugin({
          API_KEY: JSON.stringify(process.env.API_KEY)
      }),
    //   new DashboardPlugin()
  ],
  devServer: {
      contentBase: './public',
      port: 7700,
  }
};

if(isProd) {
    config.plugins.push(
        new UglifyJSPlugin(),
        new CopyWebpackPlugin([{
          from: __dirname + '/src/public'
      }])
    );
};

module.exports = config;
